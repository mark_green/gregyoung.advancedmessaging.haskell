module Convenience (
    makeBus,
    roundRobinTo
) where

import Control.Monad (forM, replicateM, forever)

import Infrastructure

makeBus :: [IO (MessageHandler m)] -> IO (MessageHandler m)
makeBus handlerConstructors = do
    handlers <- sequence handlerConstructors
    return $ multiplexingHandler handlers

roundRobinTo :: [MessageHandler m] -> IO (MessageHandler m)
roundRobinTo handlers = do
    queuedHandlers <- forM handlers queuedHandler
    queuedStatefulHandler roundRobinHandler queuedHandlers