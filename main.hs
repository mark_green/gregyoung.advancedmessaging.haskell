module Main where

import Control.Concurrent (forkIO, threadDelay, newChan, readChan, writeChan)
import Control.Monad (forM, replicateM, forever)
import GHC.Conc (setNumCapabilities, getNumProcessors)
import Data.Map (Map)
import qualified Data.Map as Map

import Infrastructure
import Convenience
import Messages
import Handlers

main = do
    -- Use all the CPU cores we can get our grubby hands on
    getNumProcessors >>= setNumCapabilities

    -- Bootstrap handlers, call it a bus
    assistantManager   <- queuedHandler assistantManagerHandler
    cashier            <- queuedStatefulHandler cashierHandler Map.empty
    cooks              <- loadBalancedHandler $ replicate 10 cookHandler
    manager            <- queuedHandler managerHandler
    waitress           <- queuedStatefulHandler waitressHandler 1
    orderManagerRunner <- queuedStatefulHandler orderManagerRunnerHandler Map.empty
    let bus = multiplexingHandler [
                    assistantManager,
                    cashier,
                    cooks,
                    manager,
                    orderManagerRunner,
                    waitress]
    
    let numberOfOrders = 200

    -- Use a channel as a queue to keep track of orderIds as we place them and pay for them
    orderIdsToPay <- newChan
    orderIdsPaid <- newChan

    -- Place some orders
    replicateM numberOfOrders $ do
        routeMessageTo waitress bus $ PlaceOrder [OrderItem {orderItemDescription="Barramundi"}] (writeChan orderIdsToPay)
    
    -- Pay for the orders
    forkIO $ forever $ do
        orderId <- readChan orderIdsToPay
        routeMessageTo cashier bus $ PayForOrder orderId (\orderPaid ->
            if orderPaid
                then writeChan orderIdsPaid orderId
                else writeChan orderIdsToPay orderId)

    -- When all orders are paid, we're done
    replicateM numberOfOrders $ readChan orderIdsPaid
    threadDelay (1*1000000)
    return ()