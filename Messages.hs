module Messages (
    Message(..),
    OrderMessageType(..),
    OrderDocument(..),
    OrderId(..),
    OrderItem(..)
) where

-- | Message
--   The type of every message in the system
data Message = OrderMessage OrderMessageType OrderDocument
             | PlaceOrder [OrderItem] (OrderId -> IO ())
             | PayForOrder OrderId (Bool -> IO ())
instance Show Message where
  show (OrderMessage orderMessageType orderDocument) = "OrderMessage "++(show orderMessageType)++" "++(show orderDocument)
  show (PlaceOrder items returnOrderId) = "PlaceOrder "++(show items)
  show (PayForOrder orderId returnWasSuccessful) = "PayForOrder "++orderId

data OrderMessageType -- EVENTS
                      = OrderPlaced
                      | OrderCooked
                      | FoodNotCookedInTime
                      | OrderPriced
                      | OrderPaid
                      | OrderComplete
                      -- COMMANDS
                      | CookFoodForOrder
                      | PriceOrder
                      | RecordOrderForPayment
                      | PrintOrder
    deriving (Show)

type OrderId = String

data OrderDocument = OrderDocument {
    orderDocumentCustomerCategory :: String,
    orderDocumentOrderId :: OrderId,
    orderDocumentItems :: [OrderItem],
    orderDocumentServer :: String,
    orderDocumentTableNumber :: String
} deriving (Show)

data OrderItem = OrderItem {
    orderItemDescription :: String
    --orderItemQuantity :: Integer,
    --orderItemPrice :: Double,
    --orderItemIngredients :: [String]
} deriving (Show)