module Handlers (
    assistantManagerHandler,
    cashierHandler,
    cookHandler,
    managerHandler,
    waitressHandler,
    okCustomerOrderManager,
    dodgyCustomerOrderManager,
    orderManagerRunnerHandler
) where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.List
import qualified Control.Monad.State as State
import System.Random
import Control.Concurrent (threadDelay)

import Infrastructure
import Messages

okCustomerOrderManager :: StatefulMessageHandler Message Bool
okCustomerOrderManager = StatefulMessageHandler handle where
    handle bus@(MessageHandler publish) (OrderMessage messageType orderDocument) =
        case messageType of
            OrderPlaced -> do
                State.lift $ publish bus $ OrderMessage CookFoodForOrder orderDocument
                State.lift $ sendAfterDelay (2*1000000) bus $ OrderMessage FoodNotCookedInTime orderDocument
            FoodNotCookedInTime -> do
                foodCooked <- State.get
                if foodCooked then
                    return ()
                else do
                    State.lift $ publish bus $ OrderMessage CookFoodForOrder orderDocument
                    State.lift $ sendAfterDelay (2*1000000) bus $ OrderMessage FoodNotCookedInTime orderDocument
            OrderCooked -> do
                State.put True
                State.lift $ publish bus $ OrderMessage PriceOrder orderDocument
            OrderPriced ->
                State.lift $ publish bus $ OrderMessage RecordOrderForPayment orderDocument
            OrderPaid ->
                State.lift $ publish bus $ OrderMessage OrderComplete orderDocument
            otherwise -> return ()
    handle bus@(MessageHandler publish) _ = return ()

dodgyCustomerOrderManager :: MessageHandler Message
dodgyCustomerOrderManager = MessageHandler handle where
    handle bus@(MessageHandler publish) (OrderMessage messageType orderDocument) =
        case messageType of
            OrderPlaced -> publish bus $ OrderMessage PriceOrder orderDocument
            OrderPriced -> publish bus $ OrderMessage RecordOrderForPayment orderDocument
            OrderPaid   -> publish bus $ OrderMessage CookFoodForOrder orderDocument
            OrderCooked -> publish bus $ OrderMessage OrderComplete orderDocument
            otherwise   -> return ()
    handle bus@(MessageHandler publish) _ = return ()

orderManagerRunnerHandler :: StatefulMessageHandler Message (Map OrderId (MessageHandler Message))
orderManagerRunnerHandler = processManagerRunnerHandler (ProcessManagerRunnerSpec getMessageType getCorrelationId getProcessManager) where
    getMessageType message@(OrderMessage messageType _) =
        case messageType of
            OrderPlaced   -> StartProcessManager
            OrderComplete -> EndProcessManager
            otherwise     -> HandleNormally
    getCorrelationId message@(OrderMessage _ orderDocument) =
        orderDocumentOrderId orderDocument
    getProcessManager message@(OrderMessage _ orderDocument) =
        case (orderDocumentCustomerCategory orderDocument) of
            "dodgy"   -> return dodgyCustomerOrderManager
            otherwise -> queuedStatefulHandler okCustomerOrderManager False

cookHandler :: MessageHandler Message
cookHandler = MessageHandler handle where
    handle bus@(MessageHandler publish) (OrderMessage CookFoodForOrder orderDocument) = do
        cookingTime <- getStdRandom (randomR (1,6))
        threadDelay (cookingTime*100000)
        publish bus $ OrderMessage OrderCooked orderDocument
    handle bus@(MessageHandler publish) _ = return ()

assistantManagerHandler :: MessageHandler Message
assistantManagerHandler = MessageHandler handle where
    handle bus@(MessageHandler publish) (OrderMessage PriceOrder orderDocument) = do
        publish bus $ OrderMessage OrderPriced orderDocument
    handle bus@(MessageHandler publish) _ = return ()

cashierHandler :: StatefulMessageHandler Message (Map OrderId OrderDocument)
cashierHandler = StatefulMessageHandler handle where
    handle bus@(MessageHandler publish) (OrderMessage RecordOrderForPayment orderDocument) = do
        let orderId = orderDocumentOrderId orderDocument
        State.modify $ Map.insert orderId orderDocument
    handle bus@(MessageHandler publish) (PayForOrder orderId returnWasSuccessful) = do
        outstandingOrders <- State.get
        if Map.member orderId outstandingOrders then do
            State.modify $ Map.delete orderId
            State.lift $ publish bus $ OrderMessage OrderPaid (outstandingOrders Map.! orderId)
            State.lift $ returnWasSuccessful True
        else do
            State.lift $ returnWasSuccessful False
    handle bus@(MessageHandler publish) _ = return ()

waitressHandler :: StatefulMessageHandler Message Int
waitressHandler = StatefulMessageHandler handle where
    handle bus@(MessageHandler publish) (PlaceOrder orderItems returnOrderId) = do
        orderId <- State.get
        State.modify (+1)
        State.lift $ publish bus $ OrderMessage OrderPlaced OrderDocument {
            orderDocumentCustomerCategory = if odd orderId then "ok" else "dodgy",
            orderDocumentOrderId = show orderId,
            orderDocumentItems = orderItems,
            orderDocumentServer = "Brenda",
            orderDocumentTableNumber = "17"
        }
        State.lift $ returnOrderId $ show orderId
    handle bus@(MessageHandler publish) _ = return ()

managerHandler :: MessageHandler Message
managerHandler = MessageHandler handle where
    handle bus@(MessageHandler publish) message@(OrderMessage OrderComplete orderDocument) = print orderDocument
    handle bus@(MessageHandler publish) _ = return ()